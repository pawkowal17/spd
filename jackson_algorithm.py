#! /usr/bin/env python3
# -*- coding: utf-8 -*-


class Task:

    def __init__(self, t_nr, t_r, t_p, sorted_task):
        self.t_nr = t_nr
        self.t_r = t_r
        self.t_p = t_p
        self.sorted_task = sorted_task


in_list = ["JACK1.DAT", "JACK2.DAT", "JACK3.DAT", "JACK4.DAT", "JACK5.DAT", "JACK6.DAT", "JACK7.DAT", "JACK8.DAT"]
out_list = ["JACK1.OUT", "JACK2.OUT", "JACK3.OUT", "JACK4.OUT", "JACK5.OUT", "JACK6.OUT", "JACK7.OUT", "JACK8.OUT"]

for i in list(range(8)):
    with open(in_list[i], "r") as f:
        data = f.readlines()

    size_list = int(data[0])
    data.remove(data[0])
    nr = 0
    task_list = []
    sorted_list = []

    for line in data:
        numbers = line.split()
        task_list.append(Task(nr, int(numbers[0]), int(numbers[1]), False))
        nr += 1
    f.close()

    for j in list(range(size_list)):
        min_r = 1000000
        for k in list(range(size_list)):
            if task_list[k].t_r < min_r and task_list[k].sorted_task is False:
                nr = task_list[k].t_nr
                min_r = task_list[k].t_r
        sorted_list.append(task_list[nr])
        task_list[nr].sorted_task = True

    m = 0
    time = 0

    while m < size_list:
        if sorted_list[m].t_r > time:
            time += 1
        else:
            time += sorted_list[m].t_p
            m += 1

    file = open(out_list[i], "w")
    file.write(str(time))
    file.close()
    print(time)
